# GitBook Pages Template

This is a template that uses GitBook to create a static web page from Markdown that is published at git-pages.thm.de.

Some content is provided below for demonstration purposes.

## Just a little content...

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

*Some unordered list:*

* Lorem ipsum dolor sit amet, consectetur adipisicing elit
* sed do eiusmod tempor incididunt ut labore et dolore magna
* aliqua. Ut enim ad minim veniam, quis nostrud exercitation
* ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute

**Some ordered list:**

1. dolor in reprehenderit in voluptate velit esse
2. cillum dolore eu fugiat nulla pariatur. Excepteur
3. sint occaecat cupidatat non proident, sunt in culpa
4. qui officia deserunt mollit anim id est laborum.

***Some Image here:***

![Old man gives thumbs up](old_man_thumb_up.jpg)

This image was published by Aziz Acharki on unsplash: [https://unsplash.com/photos/alANOC4E8iM](https://unsplash.com/photos/alANOC4E8iM). It is licensed under the [Unsplash license](https://unsplash.com/license). Thank you very much!

### Some more content

The content of the `.gitlab-ci.yml` file looks like this:

```yaml
# requiring the environment of NodeJS 10
image: node:10

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  tags:
    - nodejs
  script:
    - gitbook build . public # build to public path
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  tags:
    - nodejs
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

```

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
